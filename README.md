The Not So Fancy Product Bundle
======

How to use
----------
You'll need a [Developer Org with Salesforce CPQ](https://developer.salesforce.com/promotions/orgs/cpqtrails) and [Salesforce CLI](https://developer.salesforce.com/tools/sfdxcli).

Once you got that shiny CPQ Org up and Salesforce CLI installed you need to connect your account, you can run the following command and login through the web interface:
```
sfdx force:auth:web:login -s -d -r https://login.salesforce.com
```
Then clone the repository:
```
git clone git@gitlab.com:guhelski/tnsfpb.git
```
And deploy the source/changes to your Org, inside the project's directory run:
```
sfdx force:source:deploy -x package.xml
```

How to test
--------------
You can run all tests with:
```
sfdx force:apex:test:run -y
```
*Keep in mind that tests will not be running locally so you need to deploy your changes before running the tests, otherwise it will have no effect whatsoever.*
