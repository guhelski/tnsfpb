trigger ProductOptionTrigger on SBQQ__ProductOption__c (before insert) {

    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            ProductOptionHandler.OnBeforeInsert(Trigger.new);
        }
    }
}