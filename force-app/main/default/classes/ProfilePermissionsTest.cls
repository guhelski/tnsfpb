@isTest
private class ProfilePermissionsTest {
    @isTest
    private static void seniorProductManagerCanCreateAndChangeBundlesTest() {
        Profile seniorProductManagerProfile = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Senior Product Manager'
            LIMIT 1
        ];

        User seniorProductManagerUser = new User(
            Alias = 'spm',
            Email = 'seniorproductmanager@testpicnic.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = seniorProductManagerProfile.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = 'seniorproductmanager@testpicnic.com'
        );
        insert seniorProductManagerUser;

        System.runAs(seniorProductManagerUser) {
            List<Product2> products = new List<Product2>();
            Product2 productA = new Product2(Name='Test Product A');
            products.add(productA);

            Product2 productB = new Product2(Name='Test Product B');
            products.add(productB);

            Product2 bundle = new Product2(Name='Test Bundle');
            products.add(bundle);

            insert products;

            SBQQ__ProductOption__c bundleOptionProductA = new SBQQ__ProductOption__c(
                SBQQ__ConfiguredSKU__c=bundle.Id,
                SBQQ__OptionalSKU__c=productA.Id,
                SBQQ__Number__c=1
            );

            Database.SaveResult bundleOptionProductAResult = Database.insert(bundleOptionProductA, false);
            System.assertEquals(true, bundleOptionProductAResult.isSuccess());

            SBQQ__ProductOption__c bundleOptionProductB = new SBQQ__ProductOption__c(
                SBQQ__ConfiguredSKU__c=bundle.Id,
                SBQQ__OptionalSKU__c=productB.Id,
                SBQQ__Number__c=1
            );

            Database.SaveResult bundleOptionProductBResult = Database.insert(bundleOptionProductB, false);
            System.assertEquals(true, bundleOptionProductBResult.isSuccess());
        }
    }

    @isTest
    private static void profilesThatAreNotSeniorProductManagerCanNotCreateAndChangeBundlesTest() {
        Profile chatterFreeUserUserProfile = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Chatter Free User'
            LIMIT 1
        ];

        User chatterFreeUserUser = new User(
            Alias = 'cfu',
            Email = 'chatterfreeuser@testpicnic.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = chatterFreeUserUserProfile.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = 'chatterfreeuser@testpicnic.com'
        );
        insert chatterFreeUserUser;

        System.runAs(chatterFreeUserUser) {
            List<Product2> products = new List<Product2>();
            Product2 productA = new Product2(Name='Test Product A');
            products.add(productA);

            Product2 productB = new Product2(Name='Test Product B');
            products.add(productB);

            Product2 bundle = new Product2(Name='Test Bundle');
            products.add(bundle);

            insert products;

            SBQQ__ProductOption__c bundleOptionProductA = new SBQQ__ProductOption__c(
                SBQQ__ConfiguredSKU__c=bundle.Id,
                SBQQ__OptionalSKU__c=productA.Id,
                SBQQ__Number__c=1
            );

            Database.SaveResult bundleOptionProductAResult = Database.insert(bundleOptionProductA, false);
            System.assert(!bundleOptionProductAResult.isSuccess());

            SBQQ__ProductOption__c bundleOptionProductB = new SBQQ__ProductOption__c(
                SBQQ__ConfiguredSKU__c=bundle.Id,
                SBQQ__OptionalSKU__c=productB.Id,
                SBQQ__Number__c=1
            );

            Database.SaveResult bundleOptionProductBResult = Database.insert(bundleOptionProductB, false);
            System.assert(!bundleOptionProductBResult.isSuccess());
        }
    }
}