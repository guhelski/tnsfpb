@isTest
private class ProductOptionHandlerTest {
	@isTest
    private static void aBundleCanNotContainAnotherBundleTest() {
        List<Product2> products = new List<Product2>();
        Product2 productA = new Product2(Name='Test Product A');
        products.add(productA);

        Product2 productB = new Product2(Name='Test Product B');
        products.add(productB);

		Product2 bundleA = new Product2(Name='Test Bundle A');
        products.add(bundleA);

		Product2 bundleB = new Product2(Name='Test Bundle B');
        products.add(bundleB);

        insert products;

        SBQQ__ProductOption__c bundleAOptions = new SBQQ__ProductOption__c(
            SBQQ__ConfiguredSKU__c=bundleA.Id,
            SBQQ__OptionalSKU__c=productA.Id,
            SBQQ__Number__c=1
        );
        insert bundleAOptions;

        SBQQ__ProductOption__c bundleBOptions = new SBQQ__ProductOption__c(
            SBQQ__ConfiguredSKU__c=bundleB.Id,
            SBQQ__OptionalSKU__c=productB.Id,
            SBQQ__Number__c=1
        );
    	insert bundleBOptions;

		SBQQ__ProductOption__c bundleInsideBundle = new SBQQ__ProductOption__c(
            SBQQ__ConfiguredSKU__c=bundleA.Id,
            SBQQ__OptionalSKU__c=bundleB.Id,
            SBQQ__Number__c=1
        );

        Test.startTest();
        Database.SaveResult result = Database.insert(bundleInsideBundle, false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('A bundle can not contain another bundle.',
                            result.getErrors()[0].getMessage());
    }
}