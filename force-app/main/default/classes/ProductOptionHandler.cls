public with sharing class ProductOptionHandler {
    public static void OnBeforeInsert(List<SBQQ__ProductOption__c> newProductOptions) {
        Integer numberOfOptions = 0;

        // Get the related product options for this product.
        for (Product2 productOptions : [SELECT (SELECT SBQQ__OptionalSKU__c FROM SBQQ__Options__r) FROM Product2 WHERE Id = :newProductOptions[0].SBQQ__OptionalSKU__c]) {
            for (SBQQ__ProductOption__c productOption: productOptions.SBQQ__Options__r) {
                numberOfOptions++;
            }
        }

        if (numberOfOptions > 0) {
            newProductOptions[0].addError('A bundle can not contain another bundle.');
        }
    }
}
